<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    try {
        $p = $request->getQueryParams();
        if (empty($p['filtr'])) {
            $stmt = $this->db->query('SELECT * FROM person');
        } else {
            $stmt = $this->db->prepare(
                    'SELECT * FROM person WHERE first_name LIKE :f OR last_name LIKE :f ORDER BY last_name'
            );
            $stmt->bindValue(':f', '%' . $p['filtr'] . '%');
            $stmt->execute();
        }

        $data = $stmt->fetchAll();
        $this->view->render($response, "index.latte", [
            'osoby' => $data
        ]);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
})->setName('index');


$app->get('/new-person', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'new-person.latte');
});


$app->post('/new-person', function(Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        if (!empty($data['pr']) && !empty($data['pz']) && !empty($data['jm'])) {
            $vy = empty ($data['vy']) ? null : $data['vy'];
            $dn = empty ($data['dn']) ? null : $data['dn'];
            
            
            $stmt = $this->db->prepare('INSERT INTO person (first_name, last_name, nickname, height, birth_day, gender VALUES (:jm, :pr, :pz, :vy, :dn, :po)');
            $stmt->bindValue(':jm', $data['jm']);
            $stmt->bindValue(':pr', $data['pr']);
            $stmt->bindValue(':pz', $data['pz']);
            $stmt->bindValue(':vy', '$vy');
            $stmt->bindValue(':dn', '$dn');
            $stmt->bindValue(':po', $data['po');
          
            
            $stmt->execute();
            return $response->withHeader('Location', '.');
        } else {
            $tpiVars['error'] = 'Zadejte povinne udaje';
        }
    } catch (Exception $ex) {
        if ($ex->getCode() == 23505) {
            $tpiVars['error'] = 'Tato osoba uz je v databazi';
        }elseif ($ex->getCode() == 22007){
            $tpiVars['error'] = 'Tato osoba uz je v databazi';
        }else{
            $this->logger->error($ex->getMessage());
            
        }
        
    }
    return $this->view->render($response, 'new-person.latte', $tpiVars);
});

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');
